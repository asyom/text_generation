﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MoluClass;
using Svg;
using Svg.Transforms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {

        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var des = new MyDes();
            var data = File.ReadAllText("C:\\字帖\\zitie1.txt",Encoding.UTF8);
            var lines = data.Split(new string[] { "\r\n" }, StringSplitOptions.None);
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < lines.Length; i++)
            {
                var rols = lines[i].Split('\t');
                if (rols != null && rols.Length == 6 && rols[5].Length > 0)
                {
                    for (int j = 0; j < rols.Length; j++)
                    {
                        if (j == 0)
                        {
                            sb.Append(rols[j]);
                        }
                        else
                        {
                            if (j == 5)
                            {
                                sb.Append("\t").Append(des.Decr(rols[j]));
                            }
                            else
                            {
                                sb.Append("\t").Append(rols[j]);
                            }
                        }
                    }
                }
                else
                {
                    continue;
                }
                sb.Append("\r\n");
            }
            File.WriteAllText("C:\\字帖\\zitie_utf8.txt", sb.ToString());
        }

    }
}
