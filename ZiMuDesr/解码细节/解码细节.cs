﻿//using System;
//using System.Diagnostics;
//using System.IO;
//using System.Runtime.CompilerServices;
//using System.Security.Cryptography;
//using System.Text;
//using Microsoft.VisualBasic;
//using Microsoft.VisualBasic.CompilerServices;

//namespace MoluClass
//{
//	// Token: 0x02000036 RID: 54
//	public class MyDes
//	{
//		// Token: 0x17000051 RID: 81
//		// (get) Token: 0x0600016C RID: 364 RVA: 0x000074D7 File Offset: 0x000056D7
//		// (set) Token: 0x0600016D RID: 365 RVA: 0x000074DF File Offset: 0x000056DF
//		public string Fname { get; set; }

//		// Token: 0x14000009 RID: 9
//		// (add) Token: 0x0600016E RID: 366 RVA: 0x000074E8 File Offset: 0x000056E8
//		// (remove) Token: 0x0600016F RID: 367 RVA: 0x00007520 File Offset: 0x00005720
//		public event MyDes.ReturnStrEventHandler ReturnStr;

//		// Token: 0x1400000A RID: 10
//		// (add) Token: 0x06000170 RID: 368 RVA: 0x00007558 File Offset: 0x00005758
//		// (remove) Token: 0x06000171 RID: 369 RVA: 0x00007590 File Offset: 0x00005790
//		public event MyDes.ReChEventHandler ReCh;

//		// Token: 0x1400000B RID: 11
//		// (add) Token: 0x06000172 RID: 370 RVA: 0x000075C8 File Offset: 0x000057C8
//		// (remove) Token: 0x06000173 RID: 371 RVA: 0x00007600 File Offset: 0x00005800
//		public event MyDes.ReStEventHandler ReSt;

//		// Token: 0x06000174 RID: 372 RVA: 0x00007638 File Offset: 0x00005838
//		public MyDes()
//		{
//			this.Skey = "";
//			this.InPutS = "";
//			this.OutS = "";
//			this.Fname = "";
//			this.Skey = Conversions.ToString(this.GetLove());
//		}

//		// Token: 0x06000175 RID: 373 RVA: 0x00007688 File Offset: 0x00005888
//		public MyDes(string str, string f)
//		{
//			this.Skey = "";
//			this.InPutS = "";
//			this.OutS = "";
//			this.Fname = "";
//			this.Skey = Conversions.ToString(this.GetLove());
//			this.InPutS = str;
//			this.Fname = f;
//			new Text(f).CreatFile();
//		}

//		// Token: 0x17000052 RID: 82
//		// (get) Token: 0x06000176 RID: 374 RVA: 0x000076F1 File Offset: 0x000058F1
//		// (set) Token: 0x06000177 RID: 375 RVA: 0x000076F9 File Offset: 0x000058F9
//		public string TheKay
//		{
//			get
//			{
//				return this.Skey;
//			}
//			set
//			{
//				this.Skey = value;
//			}
//		}

//		// Token: 0x17000053 RID: 83
//		// (get) Token: 0x06000178 RID: 376 RVA: 0x000076F1 File Offset: 0x000058F1
//		// (set) Token: 0x06000179 RID: 377 RVA: 0x00007702 File Offset: 0x00005902
//		public string TheKey
//		{
//			get
//			{
//				return this.Skey;
//			}
//			set
//			{
//				this.Skey = this.MD5Y(value, 32);
//			}
//		}

//		// Token: 0x0600017A RID: 378 RVA: 0x00007714 File Offset: 0x00005914
//		public void WriteStr()
//		{
//			new Encryptor(Strings.Mid(this.Skey, checked((int)Math.Round((double)(Strings.Len(this.Skey) - 8) / 2.0)), 8)).EncSaveFile(this.encrypt(), this.Fname);
//			MyDes.ReturnStrEventHandler returnStrEvent = this.ReturnStrEvent;
//			if (returnStrEvent != null)
//			{
//				returnStrEvent("SaveOk");
//			}
//		}

//		// Token: 0x0600017B RID: 379 RVA: 0x00007778 File Offset: 0x00005978
//		public void ReadStr()
//		{
//			try
//			{
//				Encryptor encryptor = new Encryptor(Strings.Mid(this.Skey, checked((int)Math.Round((double)(Strings.Len(this.Skey) - 8) / 2.0)), 8));
//				MyDes.ReturnStrEventHandler returnStrEvent = this.ReturnStrEvent;
//				if (returnStrEvent != null)
//				{
//					returnStrEvent(this.decrypt(encryptor.EncGetFile(this.Fname)));
//				}
//			}
//			catch (Exception ex)
//			{
//				Interaction.MsgBox(ex.ToString(), MsgBoxStyle.OkOnly, null);
//			}
//		}

//		// Token: 0x0600017C RID: 380 RVA: 0x00007808 File Offset: 0x00005A08
//		public void ReadCh()
//		{
//			checked
//			{
//				try
//				{
//					Encryptor encryptor = new Encryptor(Strings.Mid(this.Skey, (int)Math.Round((double)(Strings.Len(this.Skey) - 8) / 2.0), 8));
//					Hanzi hanzi = new Hanzi();
//					hanzi.Han = Strings.Mid(this.Fname, Strings.InStrRev(this.Fname, "\\", -1, CompareMethod.Binary) + 1, 1);
//					hanzi.Path = this.decrypt(encryptor.EncGetFile(this.Fname));
//					MyDes.ReChEventHandler reChEvent = this.ReChEvent;
//					if (reChEvent != null)
//					{
//						reChEvent(hanzi);
//					}
//				}
//				catch (Exception ex)
//				{
//					Trace.WriteLine(ex.ToString());
//				}
//			}
//		}

//		// Token: 0x0600017D RID: 381 RVA: 0x000078C8 File Offset: 0x00005AC8
//		public void ReadSty()
//		{
//			try
//			{
//				Encryptor encryptor = new Encryptor(Strings.Mid(this.Skey, checked((int)Math.Round((double)(Strings.Len(this.Skey) - 8) / 2.0)), 8));
//				new Spage();
//				string str = this.decrypt(encryptor.EncGetFile(this.Fname));
//				MyDes.ReturnStrEventHandler returnStrEvent = this.ReturnStrEvent;
//				if (returnStrEvent != null)
//				{
//					returnStrEvent(str);
//				}
//			}
//			catch (Exception ex)
//			{
//				Trace.WriteLine(ex.ToString());
//			}
//		}

//		// Token: 0x0600017E RID: 382 RVA: 0x0000795C File Offset: 0x00005B5C
//		public object ReadStrFn()
//		{
//			Encryptor encryptor = new Encryptor(Strings.Mid(this.Skey, checked((int)Math.Round((double)(Strings.Len(this.Skey) - 8) / 2.0)), 8));
//			return this.decrypt(encryptor.EncGetFile(this.Fname));
//		}

//		// Token: 0x0600017F RID: 383 RVA: 0x000079AC File Offset: 0x00005BAC
//		public string encrypt()
//		{
//			DESCryptoServiceProvider descryptoServiceProvider = new DESCryptoServiceProvider();
//			byte[] bytes = Encoding.Default.GetBytes(this.InPutS);
//			descryptoServiceProvider.Key = Encoding.ASCII.GetBytes(Strings.Left(this.Skey, 8));
//			Trace.WriteLine("DES.Key=" + Strings.Left(this.Skey, 8));
//			descryptoServiceProvider.IV = Encoding.ASCII.GetBytes(Strings.Right(this.Skey, 8));
//			MemoryStream memoryStream = new MemoryStream();
//			CryptoStream cryptoStream = new CryptoStream(memoryStream, descryptoServiceProvider.CreateEncryptor(), CryptoStreamMode.Write);
//			cryptoStream.Write(bytes, 0, bytes.Length);
//			cryptoStream.FlushFinalBlock();
//			StringBuilder stringBuilder = new StringBuilder();
//			foreach (byte b in memoryStream.ToArray())
//			{
//				stringBuilder.AppendFormat("{0:x2}", b);
//			}
//			return stringBuilder.ToString();
//		}

//		// Token: 0x06000180 RID: 384 RVA: 0x00007A88 File Offset: 0x00005C88
//		public string encrypt(string s)
//		{
//			DESCryptoServiceProvider descryptoServiceProvider = new DESCryptoServiceProvider();
//			byte[] bytes = Encoding.Default.GetBytes(s);
//			descryptoServiceProvider.Key = Encoding.ASCII.GetBytes(Strings.Left(this.Skey, 8));
//			descryptoServiceProvider.IV = Encoding.ASCII.GetBytes(Strings.Right(this.Skey, 8));
//			MemoryStream memoryStream = new MemoryStream();
//			CryptoStream cryptoStream = new CryptoStream(memoryStream, descryptoServiceProvider.CreateEncryptor(), CryptoStreamMode.Write);
//			cryptoStream.Write(bytes, 0, bytes.Length);
//			cryptoStream.FlushFinalBlock();
//			StringBuilder stringBuilder = new StringBuilder();
//			foreach (byte b in memoryStream.ToArray())
//			{
//				stringBuilder.AppendFormat("{0:x2}", b);
//			}
//			return stringBuilder.ToString();
//		}

//		// Token: 0x06000181 RID: 385 RVA: 0x00007B44 File Offset: 0x00005D44
//		private int Unix(string t)
//		{
//			DateTime d = TimeZone.CurrentTimeZone.ToLocalTime(new DateTime(1970, 1, 1));
//			return checked((int)Math.Round((Conversions.ToDate(t) - d).TotalSeconds));
//		}

//		// Token: 0x06000182 RID: 386 RVA: 0x00007B84 File Offset: 0x00005D84
//		public string Encr(object s)
//		{
//			this.Skey = this.MD5Y(Conversions.ToString(this.Unix(DateAndTime.Now.ToString())), 16);
//			string text = this.encrypt(Conversions.ToString(s));
//			int num = Strings.Asc(Strings.Left(text, 1));
//			return Strings.Left(text, num) + this.Skey + Strings.Right(text, checked(Strings.Len(text) - num));
//		}

//		// Token: 0x06000183 RID: 387 RVA: 0x00007BF4 File Offset: 0x00005DF4
//		public string Decr(object s)
//		{
//			checked
//			{
//				string result;
//				try
//				{
//					if (Strings.Len(RuntimeHelpers.GetObjectValue(s)) <= 8)
//					{
//						result = Conversions.ToString(s);
//					}
//					else
//					{
//						int num = Strings.Asc(Strings.Left(Conversions.ToString(s), 1));
//						this.Skey = Strings.Mid(Conversions.ToString(s), num + 1, 16);
//						string s2 = Strings.Left(Conversions.ToString(s), num) + Strings.Right(Conversions.ToString(s), Strings.Len(RuntimeHelpers.GetObjectValue(s)) - num - 16);
//						result = this.decrypt(s2);
//					}
//				}
//				catch (Exception ex)
//				{
//					result = Conversions.ToString(s);
//				}
//				return result;
//			}
//		}

//		// Token: 0x06000184 RID: 388 RVA: 0x00007CA0 File Offset: 0x00005EA0
//		public string 解密(object s)
//		{
//			checked
//			{
//				string result;
//				try
//				{
//					if (Strings.Len(RuntimeHelpers.GetObjectValue(s)) <= 8)
//					{
//						result = Conversions.ToString(s);
//					}
//					else
//					{
//						int num = Strings.Asc(Strings.Left(Conversions.ToString(s), 1));
//						this.Skey = Strings.Mid(Conversions.ToString(s), num + 1, 16);
//						string s2 = Strings.Left(Conversions.ToString(s), num) + Strings.Right(Conversions.ToString(s), Strings.Len(RuntimeHelpers.GetObjectValue(s)) - num - 16);
//						result = this.decrypt(s2);
//					}
//				}
//				catch (Exception ex)
//				{
//					result = Conversions.ToString(s);
//				}
//				return result;
//			}
//		}

//		// Token: 0x06000185 RID: 389 RVA: 0x00007D4C File Offset: 0x00005F4C
//		public string decrypt(string s)
//		{
//			int num;
//			string @string;
//			int num7;
//			object obj;
//			try
//			{
//			IL_00:
//				ProjectData.ClearProjectError();
//				num = 1;
//			IL_07:
//				int num2 = 2;
//				string text = "";
//			IL_10:
//				num2 = 3;
//				int length = s.Length;
//				checked
//				{
//					for (int i = 1; i <= length; i++)
//					{
//					IL_1F:
//						num2 = 4;
//						string text2 = Strings.Mid(s, i, 1);
//					IL_2C:
//						num2 = 5;
//						if (Strings.Asc(text2) == 0)
//						{
//							break;
//						}
//					IL_37:
//						num2 = 7;
//						text += text2;
//					IL_44:
//						num2 = 8;
//					}
//				IL_52:
//					num2 = 9;
//					DESCryptoServiceProvider descryptoServiceProvider = new DESCryptoServiceProvider();
//				IL_5C:
//					num2 = 10;
//					int num3 = (int)Math.Round(unchecked((double)text.Length / 2.0 - 1.0));
//				IL_83:
//					num2 = 11;
//					byte[] array = new byte[num3 + 1];
//				IL_91:
//					num2 = 12;
//					int num4 = num3;
//					for (int j = 0; j <= num4; j++)
//					{
//					IL_9D:
//						num2 = 13;
//						int num5 = Convert.ToInt32(text.Substring(j * 2, 2), 16);
//					IL_B5:
//						num2 = 14;
//						array[j] = (byte)num5;
//					IL_C0:
//						num2 = 15;
//					}
//				IL_CF:
//					num2 = 16;
//					descryptoServiceProvider.Key = Encoding.ASCII.GetBytes(Strings.Left(this.Skey, 8));
//				IL_EF:
//					num2 = 17;
//					descryptoServiceProvider.IV = Encoding.ASCII.GetBytes(Strings.Right(this.Skey, 8));
//				IL_10F:
//					num2 = 18;
//					MemoryStream memoryStream = new MemoryStream();
//				IL_119:
//					num2 = 19;
//					CryptoStream cryptoStream = new CryptoStream(memoryStream, descryptoServiceProvider.CreateDecryptor(), CryptoStreamMode.Write);
//				IL_12D:
//					num2 = 20;
//					cryptoStream.Write(array, 0, array.Length);
//				IL_13E:
//					num2 = 21;
//					cryptoStream.FlushFinalBlock();
//				IL_148:
//					num2 = 22;
//					@string = Encoding.Default.GetString(memoryStream.ToArray());
//				IL_15D:
//					goto IL_20B;
//				IL_162:;
//				}
//				int num6 = num7 + 1;
//				num7 = 0;
//				@switch(ICSharpCode.Decompiler.ILAst.ILLabel[], num6);
//			IL_1CC:
//				goto IL_200;
//			IL_1CE:
//				num7 = num2;
//				@switch(ICSharpCode.Decompiler.ILAst.ILLabel[], num);
//			IL_1DE:;
//			}
//			catch when (endfilter(obj is Exception & num != 0 & num7 == 0))
//			{
//				Exception ex = (Exception)obj2;
//				goto IL_1CE;
//			}
//		IL_200:
//			throw ProjectData.CreateProjectError(-2146828237);
//		IL_20B:
//			if (num7 != 0)
//			{
//				ProjectData.ClearProjectError();
//			}
//			return @string;
//		}

//		// Token: 0x06000186 RID: 390 RVA: 0x00007F8C File Offset: 0x0000618C
//		public static string decrypt(string pToDecrypt, string sKey)
//		{
//			DESCryptoServiceProvider descryptoServiceProvider = new DESCryptoServiceProvider();
//			checked
//			{
//				int num = (int)Math.Round(unchecked((double)pToDecrypt.Length / 2.0 - 1.0));
//				byte[] array = new byte[num + 1];
//				int num2 = num;
//				for (int i = 0; i <= num2; i++)
//				{
//					int num3 = Convert.ToInt32(pToDecrypt.Substring(i * 2, 2), 16);
//					array[i] = (byte)num3;
//				}
//				descryptoServiceProvider.Key = Encoding.ASCII.GetBytes(sKey);
//				descryptoServiceProvider.IV = Encoding.ASCII.GetBytes(sKey);
//				MemoryStream memoryStream = new MemoryStream();
//				CryptoStream cryptoStream = new CryptoStream(memoryStream, descryptoServiceProvider.CreateDecryptor(), CryptoStreamMode.Write);
//				cryptoStream.Write(array, 0, array.Length);
//				cryptoStream.FlushFinalBlock();
//				return Encoding.Default.GetString(memoryStream.ToArray());
//			}
//		}

//		// Token: 0x06000187 RID: 391 RVA: 0x00008048 File Offset: 0x00006248
//		public string MD5Y(string strSource, short Code)
//		{
//			byte[] bytes = new ASCIIEncoding().GetBytes(strSource);
//			byte[] array = ((HashAlgorithm)CryptoConfig.CreateFromName("MD5")).ComputeHash(bytes);
//			string text = "";
//			checked
//			{
//				if (Code != 16)
//				{
//					if (Code != 32)
//					{
//						int num = 0;
//						do
//						{
//							text += Conversion.Hex(array[num]).PadLeft(2, '0').ToLower();
//							num++;
//						}
//						while (num <= 15);
//					}
//					else
//					{
//						int num = 0;
//						do
//						{
//							text += Conversion.Hex(array[num]).PadLeft(2, '0').ToLower();
//							num++;
//						}
//						while (num <= 15);
//					}
//				}
//				else
//				{
//					int num = 4;
//					do
//					{
//						text += Conversion.Hex(array[num]).PadLeft(2, '0').ToLower();
//						num++;
//					}
//					while (num <= 11);
//				}
//				return text;
//			}
//		}

//		// Token: 0x06000188 RID: 392 RVA: 0x00008108 File Offset: 0x00006308
//		private object GetLove()
//		{
//			return this.MD5Y("myLoveIsJiaQiaoHui", 32);
//		}

//		// Token: 0x04000081 RID: 129
//		private string Skey;

//		// Token: 0x04000082 RID: 130
//		private string InPutS;

//		// Token: 0x04000083 RID: 131
//		private string OutS;

//		// Token: 0x02000097 RID: 151
//		// (Invoke) Token: 0x06000499 RID: 1177
//		public delegate void ReturnStrEventHandler(string str);

//		// Token: 0x02000098 RID: 152
//		// (Invoke) Token: 0x0600049D RID: 1181
//		public delegate void ReChEventHandler(Hanzi Zi);

//		// Token: 0x02000099 RID: 153
//		// (Invoke) Token: 0x060004A1 RID: 1185
//		public delegate void ReStEventHandler(Spage St);
//	}
//}
