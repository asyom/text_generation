# 字模与笔画生成工具

### 功能

根据输入的内容生成对应文字的字模图片或字模笔画图片。



### 预览图

* 程序界面

<img src="https://images.gitee.com/uploads/images/2020/0826/105951_c84048e7_1625744.jpeg" alt="preview1" style="zoom:60%;" />



* 字模图片生成结果

<img src="https://images.gitee.com/uploads/images/2020/0826/110107_7abe1270_1625744.jpeg" alt="preview1" style="zoom:56%;" />



## 具体功能

* 设置字体边长：可控制生成的字体大小
* 设置字体描边大小：在字体外生成描边

* 生成笔画：勾选后可以生成从第一笔到最后完整字体的所有内容

* 填充色：文字主体颜色
* 描边色：文字边框颜色
* 背景色：文字背景填充颜色

* 透明通道保留：生成的图片格式为Png，将保留图像的透明通道，便于二次处理


<img src="https://images.gitee.com/uploads/images/2020/0826/110125_23fa6f44_1625744.jpeg" alt="setting1" style="zoom:60%;" />


<img src="https://images.gitee.com/uploads/images/2020/0826/110134_ff07a694_1625744.jpeg" alt="setting2" style="zoom:60%;" />




### 项目介绍

* ZiMuGenerator

  字模与笔画生成工具主程序，用于渲染并生成字模。

* ZiMuDesr

  字模解码工具，该工具字模库解码自“易字帖”，在该项目“解码细节”目录内有相关解码的步骤



### 项目构建

1. 在Visual Studio中（本项目使用VS2019）克隆本项目

2. 右键ZiMuGenerator项目，选择Manager Nuget Packages，搜索并安装SVG和Costura.Fody库。如果Nuget中无法安装，可到下方链接下载。

   * SVG（在本项目使用3.1.1版）

     [Github](https://github.com/vvvv/SVG)、[Nuget](https://www.nuget.org/packages/Svg/3.1.1?_src=template)


   * Costura.Fody（在本项目使用4.1.0版）

     [Github](https://github.com/Fody/Costura) 、[Nuget](https://www.nuget.org/packages/Costura.Fody/4.1.0?_src=template)

   

### 项目发布

*注：本项目使用Costura.Fody对DLL进行了整合，编译生成的EXE已内嵌了所有运行所需的DLL和资源文件，若想将本程序安装到其他机器，只需复制EXE一个程序即可。*

1. 设置Solution Configurations为Release（通常可在VS顶部快捷栏切换Debug和Release）
2. VS菜单栏点击Build→Rebuild
3. 在ZiMuGenerator\bin\Release中可找到生成的ZiMuGenerator.exe文件，将其拷贝至任何一台Windows系统电脑均可运行（需要.Net Framwork版本在4.5.2或以上）

