﻿using Svg;
using Svg.Transforms;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Windows.Shapes;

namespace ZiMuGenerator
{
    public class ZiMu
    {
        public char Word { set; get; }//爱
        public string PinYin { set; get; }//4i
        public string Radical { set; get; }//爫
        public string Structure { set; get; }//上中下结构
        public string SvgPath { set; get; }

        private const int NOT_FOUND = -1;

        public static bool TryParse(string line, out ZiMu item)
        {
            ZiMu result = new ZiMu();
            try
            {
                var cols = line.Split('\t');
                result.Word = cols[1][0];
                result.PinYin = cols[2];
                result.Radical = cols[3];
                result.Structure = cols[4];
                result.SvgPath = cols[5];
            }
            catch (Exception)
            {
                item = null;
                return false;
            }
            item = result;
            return true;
        }

        public Bitmap GetBitmap() => GetBitmap(SvgPath, 80);

        public Bitmap GetBitmap(int size) => GetBitmap(SvgPath, size);

        public System.Drawing.Brush BackgroundColor { set; get; } = System.Drawing.Brushes.Transparent;
        public SvgColourServer FillColor { set; get; } = new SvgColourServer(System.Drawing.Color.Red);
        public SvgColourServer StrokeColor { set; get; } = new SvgColourServer(System.Drawing.Color.Black);
        public float StrokeWidth { set; get; } = 1;

        private static readonly SvgColourServer m_TransparentSvgColourServer
            = new SvgColourServer(System.Drawing.Color.Transparent);

        public Bitmap GetBitmap(string path, int size)
        {
            StringBuilder sb = new StringBuilder(Properties.Resources.SvgTemplate);
            sb.Replace("【边长】", size.ToString());
            sb.Replace("【路径】", path);

            //修正图像偏移
            SvgDocument document = SvgDocument.FromSvg<SvgDocument>(sb.ToString());
            document.Transforms = new SvgTransformCollection();
            document.Transforms.Add(new SvgScale(1, -1));
            document.Transforms.Add(new SvgTranslate(0, -size + 100.0f * size / 1000.0f));
            document.Transforms.Add(new SvgScale(size / 1000.0f, size / 1000.0f));

            //构建画布
            Bitmap bitmap = new Bitmap(size, size);
            using (var g = Graphics.FromImage(bitmap))
            {
                //画背景
                g.FillRectangle(BackgroundColor, new RectangleF(0, 0, size, size));

                //画边框
                document.StrokeWidth = StrokeWidth;
                document.Stroke = StrokeColor;
                document.Fill = m_TransparentSvgColourServer;
                document.Draw(g, new SizeF(size, size));

                //填充字体
                document.Stroke = null;
                document.Fill = FillColor;
                document.Draw(g, new SizeF(size, size));
            }

            return bitmap;
        }

        public Bitmap[] GetBitmaps(int size)
        {
            List<string> paths = new List<string>();

            //拆解结构
            if (SvgPath.IndexOf("ZM") != NOT_FOUND)
            {
                string[] lines = SvgPath.Split(new string[] { "ZM" }, StringSplitOptions.None);
                for (int i = 0; i < lines.Length - 1; i++)
                {
                    if (i == 0)
                    {
                        paths.Add(lines[i] + "Z");
                    }
                    else
                    {
                        paths.Add("M" + lines[i]);
                    }
                }
            }
            paths.Add(SvgPath);//完整一个字


            Bitmap[] bmps = new Bitmap[paths.Count];
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < paths.Count; i++)
            {
                sb.Append(paths[i]);
                bmps[i] = GetBitmap(sb.ToString(), size);
            }

            return bmps;
        }
    }
}
