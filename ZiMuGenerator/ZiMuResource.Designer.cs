﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ZiMuGenerator {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "16.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class ZiMuResource {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal ZiMuResource() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("ZiMuGenerator.ZiMuResource", typeof(ZiMuResource).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 2	阿	1|9	阝	左右结构	M 228 673 Q 265 680 303 697 Q 319 707 331 696 Q 334 692 310 620 Q 289 563 297 549 Q 325 521 347 457 Q 353 439 345 430 Q 338 420 312 425 Q 281 432 248 435 Q 224 436 250 421 Q 310 391 363 353 Q 378 340 391 353 Q 406 363 412 389 Q 424 444 358 527 Q 318 567 343 605 Q 386 675 411 694 Q 430 707 421 720 Q 408 739 369 758 Q 347 767 333 754 Q 279 715 219 704 L 228 673 ZM 219 704 Q 173 734 155 739 Q 143 742 137 731 Q 133 724 142 712 Q 172 642 173 567 Q 182 335 163 208 Q 154 163 140 117 Q 131 86 137 64  [rest of string was truncated]&quot;;.
        /// </summary>
        internal static string zitie_utf8 {
            get {
                return ResourceManager.GetString("zitie_utf8", resourceCulture);
            }
        }
    }
}
