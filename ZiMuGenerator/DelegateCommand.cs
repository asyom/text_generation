﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace ZiMuGenerator
{
    public class DelegateCommand : ICommand
    {
        public event EventHandler CanExecuteChanged;

        public Action ExecuteAction { set; get; } = DefaultAction;
        public Func<bool> CanExecuteFunc { set; get; } = DefaultFunc;

        private static readonly Action DefaultAction = () => { };
        private static readonly Func<bool> DefaultFunc = () => true;

        public bool CanExecute(object parameter) => CanExecuteFunc();
        public void Execute(object parameter) => ExecuteAction();
        public void OnUpdateCanExecute() => CanExecuteChanged?.Invoke(this, null);
    }


    public class DelegateCommand<T> : ICommand
    {
        public event EventHandler CanExecuteChanged;

        public Action<T> ExecuteAction { set; get; } = DefaultAction;
        public Func<T, bool> CanExecuteFunc { set; get; } = DefaultFunc;

        private static readonly Action<T> DefaultAction = (a) => { };
        private static readonly Func<T, bool> DefaultFunc = (a) => true;

        public bool CanExecute(object parameter) => CanExecuteFunc((T)parameter);
        public void Execute(object parameter) => ExecuteAction((T)parameter);
        public void OnUpdateCanExecute() => CanExecuteChanged?.Invoke(this, null);
    }
}
