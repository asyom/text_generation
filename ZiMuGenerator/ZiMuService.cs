﻿using Svg;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace ZiMuGenerator
{
    public class ZiMuService
    {
        public Dictionary<char, ZiMu> ZiMuDictionary { private set; get; }

        public event EventHandler<int> InitProgressChanged;
        public event EventHandler<string> InitException;
        public event EventHandler InitFinished;

        public event EventHandler<int> SaveProgressChanged;
        public event EventHandler<string> SaveException;
        public event EventHandler SaveFinished;

        private int m_Progress;

        public bool IsInitFinished { private set; get; }
        public bool IsSaveFinished { private set; get; } = true;
        public bool IsGenerateAllPaths { set; get; } = true;
        public int WordSize { set; get; }
        public float StrokeWidth { set; get; } = 1;
        public SvgColourServer FillColor { set; get; } = new SvgColourServer(Color.Red);
        public Brush BackgroundColor { set; get; } = Brushes.Transparent;
        public SvgColourServer StrokeColor { set; get; } = new SvgColourServer(Color.Black);
        public string SaveRoot { set; get; }

        public void BeginInit()
        {
            Task.Run(new Action(() =>
            {
                Init();
            }));
        }

        private void Init()
        {
            try
            {
                ZiMuDictionary = new Dictionary<char, ZiMu>();

                var data = Config.ZiMuData;
                OnProgressChanged(true, false, 0);

                var lines = data.Split(new string[] { "\r\n" }, StringSplitOptions.None);
                OnProgressChanged(true, true, 0);

                var total = lines.Length;
                for (int i = 0; i < lines.Length; i++)
                {
                    if (ZiMu.TryParse(lines[i], out ZiMu zimu))
                    {
                        ZiMuDictionary.Add(zimu.Word, zimu);
                    }
                    OnProgressChanged(true, true, (int)((i + 1.0) / total * 100));
                }
            }
            catch (Exception ex)
            {
                InitException?.Invoke(this, ex.Message);
            }

            IsInitFinished = true;
            InitFinished?.Invoke(this, null);
        }

        private void OnProgressChanged(bool hasReadFile, bool hasSplitLines, int readFileProgress)
        {
            var progress = hasReadFile ? 5 : 0;
            progress += hasSplitLines ? 5 : 0;
            progress += (int)(readFileProgress * 0.9);

            if (m_Progress != progress)
            {
                m_Progress = progress;
                InitProgressChanged?.Invoke(this, m_Progress);
            }
        }

        public void ParseAndSave(string text)
        {
            if (text == null || text.Length == 0 || ZiMuDictionary == null || ZiMuDictionary.Count == 0)
            {
                return;
            }

            Task.Run(new Action(() =>
            {
                IsSaveFinished = false;
                SaveProgressChanged?.Invoke(this, 0);

                try
                {
                    for (int i = 0; i < text.Length; i++)
                    {
                        if (!ZiMuDictionary.ContainsKey(text[i]))
                        {
                            continue;
                        }

                        var zimu = ZiMuDictionary[text[i]];
                        zimu.FillColor = FillColor;
                        zimu.BackgroundColor = BackgroundColor;
                        zimu.StrokeColor = StrokeColor;
                        zimu.StrokeWidth = StrokeWidth;

                        Bitmap[] bitmaps;
                        if (IsGenerateAllPaths)
                        {
                            bitmaps = zimu.GetBitmaps(WordSize);
                        }
                        else
                        {
                            bitmaps = new Bitmap[] { zimu.GetBitmap(WordSize) };
                        }
                        for (int j = 0; j < bitmaps.Length; j++)
                        {
                            var path = $"{SaveRoot}\\[{(i + 1).ToString().PadLeft(4, '0')}]{text[i]}_{(j + 1).ToString().PadLeft(2, '0')}.png";
                            bitmaps[j].Save(path);
                        }

                        SaveProgressChanged?.Invoke(this, (int)((i + 1.0) / text.Length * 100.0));
                    }
                }
                catch (Exception ex)
                {
                    SaveException?.Invoke(this, ex.Message);
                }

                IsSaveFinished = true;
                SaveFinished?.Invoke(this, null);
            }));
        }

        public Bitmap GetPreviewImage(char word)
        {
            if (!IsInitFinished)
            {
                return null;
            }

            if (!ZiMuDictionary.ContainsKey(word))
            {
                return null;
            }

            var zimu = ZiMuDictionary[word];
            zimu.FillColor = FillColor;
            zimu.BackgroundColor = BackgroundColor;
            zimu.StrokeColor = StrokeColor;
            zimu.StrokeWidth = StrokeWidth;

            Bitmap[] bitmaps;
            if (IsGenerateAllPaths)
            {
                bitmaps = zimu.GetBitmaps(WordSize);
            }
            else
            {
                bitmaps = new Bitmap[] { zimu.GetBitmap(WordSize) };
            }

            var width = WordSize * bitmaps.Length;
            var bmp = new Bitmap(width, bitmaps[0].Height);
            using (var g = Graphics.FromImage(bmp))
            {
                for (int i = 0; i < bitmaps.Length; i++)
                {
                    g.DrawImage(bitmaps[i], new PointF(i * WordSize, 0));
                }
            }

            return bmp;
        }

    }
}
