﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZiMuGenerator
{
    public static class Config
    {
        public static string ZiMuData { set; get; } = ZiMuResource.zitie_utf8;
    }
}
